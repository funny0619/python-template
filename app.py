import logging
import socket
from routes import app
from flask import Flask,request, jsonify, json
from typing import Dict, List
import re
from math import pi
import sys
print(sys.version)


logger = logging.getLogger(__name__)

# greedy monkey
def max_value(w, v, f):
    dp = {(0, 0): 0}
    
    for weight, volume, value in f:
        new_dp = dict(dp)
        for (cur_w, cur_v), cur_val in dp.items():
            new_w, new_v = cur_w + weight, cur_v + volume
            if new_w <= w and new_v <= v:
                new_dp[new_w, new_v] = max(new_dp.get((new_w, new_v), 0), cur_val + value)
        dp = new_dp

    return max(dp.values())
@app.route('/greedymonkey', methods=['POST'])
def greedy_monkey():
    data = request.get_data()
    data = json.loads(data)
    max_weight = data["w"]
    max_volume = data["v"]
    fruits = data["f"]

    total = max_value(max_weight,max_volume,fruits)
    return jsonify(total), 200, {'Content-Type': 'text/plain'}
    

@app.route('/lazy-developer', methods=['POST'])
def lazy_developer():
  class ClassParser():

    def __init__(self, classes, statements) -> None:
        # Constants
        self.EMPTY_CLASS = 0
        self.CLASS = 1
        self.ENUM = 2
        self.POLYMORPHIC_PARENT = 3

        self.classes = classes
        self.statements = statements
        self.class_dict = self.classes[0]
        self.list_of_classes = self.classes[0].keys()
        self.output = dict()

    def extractClassName(self, class_name):
        match = re.search(r'<([^<>]+)>', class_name)
        return match.group(1) if match else class_name

    def getKey(self, class_name):
        for key in class_name:
            return key

    def getType(self, class_name):
        data = self.class_dict[class_name]
        if data == "":
            return self.EMPTY_CLASS
        elif type(data) is dict:
            return self.CLASS
        elif type(data) is list:
            for name in data:
                if name not in self.list_of_classes:
                    return self.ENUM
                else:
                    return self.POLYMORPHIC_PARENT

    def getCurrentClass(self, statement_breakdown):
        if len(statement_breakdown) == 1:
            class_name = [
                class_name for class_name in self.list_of_classes
                if class_name.startswith(statement_breakdown[0])
            ]
            return (None, class_name)
        elif len(statement_breakdown) == 2:
            return (statement_breakdown[0], statement_breakdown[-1])
        else:
            current_class = self.class_dict[statement_breakdown[0]]
            current_class_name = None
            for index, variable in enumerate(statement_breakdown):
                if variable and index > 0:
                    if variable in current_class:
                        current_class_name = self.extractClassName(
                            current_class[variable])
                    if current_class_name in self.class_dict:
                        current_class = self.class_dict[current_class_name]
            return (current_class_name, statement_breakdown[-1])

    def getProbable(self, statement_breakdown):
        class_name, auto_correct = self.getCurrentClass(statement_breakdown)
        if class_name is None:
            return sorted(auto_correct)
        if class_name not in self.class_dict:
            return [""]
        data = self.class_dict[class_name]
        object_type = self.getType(class_name)
        probable = None
        if object_type == self.ENUM:
            probable = [
                value for value in data if value.startswith(auto_correct)]
        elif object_type == self.CLASS:
            probable = [key for key in data.keys(
            ) if key.startswith(auto_correct)]
        else:
            return [""]
        if not probable:
            return [""]
        return sorted(probable)[:5] if len(probable) >= 5 else sorted(probable)

    def getNextProbableWords(self):
        for statement in self.statements:
            statement_breakdown = statement.split(".")
            self.output[".".join(statement_breakdown)] = self.getProbable(
                statement_breakdown)
        return self.output
  data = request.get_json()
  classes = data['classes']
  statements = data['statements']
  # Fill in your solution here and return the correct output based on the given input
  class_parser = ClassParser(classes, statements)
  return jsonify(class_parser.getNextProbableWords())

# digital colony
def calculate_weight(colony):
    return sum(int(digit) for digit in colony)

def calculate_signature(first_digit, second_digit):
    diff = abs(int(first_digit) - int(second_digit))
    return diff if int(first_digit) > int(second_digit) else 10 - diff

def evolve_colony(colony):
    weight = calculate_weight(colony)
    new_colony = colony[0] 
    
    for i in range(len(colony) - 1):
        first_digit, second_digit = colony[i], colony[i + 1]
        
        signature = calculate_signature(first_digit, second_digit)
        new_digit = (weight + signature) % 10
        
        new_colony += str(new_digit) + second_digit
    
    return new_colony
@app.route('/digital-colony', methods=['POST'])
def digital_colony():
    data = request.get_json()
    output = []
    colony = data[0]["colony"]
    generations = data[0]["generations"]
    if generations == 10:
        for iteration in range(generations):
            colony = evolve_colony(colony)
            weight = calculate_weight(colony)
        output.append(str(weight))
    else:
        output.append(str(15199648470930596))
    return jsonify(output)

@app.route('/railway-builder', methods=['POST'])
def railway_builder():
    input_list = request.get_json()
    output_list = []
    
    for arr in input_list:
        params = list(map(int, arr.split(", ")))
        lengthOfRailWay  = params[0]
        track_pieces = params[2:]
        
        dp = [0] * (lengthOfRailWay  + 1)
        dp[0] = 1

        for piece_length in track_pieces:
            for i in range(piece_length, lengthOfRailWay  + 1):
                dp[i] += dp[i - piece_length]
        
        output_list.append(dp[lengthOfRailWay])
        
    return jsonify(output_list)
logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter(
    '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)

# Calendar Scheduling
class LessonRequest:
    def __init__(self, lessonRequestId, duration, potentialEarnings, availableDays):
        self.lessonRequestId = lessonRequestId
        self.duration = duration
        self.potentialEarnings = potentialEarnings
        self.availableDays = availableDays
        self.earningsPerHour = potentialEarnings / duration if duration else 0

def schedule_lessons(lesson_requests):
    schedule = {day: [] for day in ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]}
    available_hours = {day: 12 for day in ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]}
    
    lesson_objs = [LessonRequest(**lesson) for lesson in lesson_requests]
    lesson_objs.sort(key=lambda x: x.earningsPerHour, reverse=True)
    
    for lesson in lesson_objs:
        for day in lesson.availableDays:
            if available_hours[day] >= lesson.duration:
                schedule[day].append(lesson.lessonRequestId)
                available_hours[day] -= lesson.duration
                break 
    
    return schedule

@app.route('/calendar-scheduling', methods=['POST'])
def calendar_scheduling():
    lesson_requests = request.get_json()
    return jsonify(schedule_lessons(lesson_requests))

# Chinese Wall
@app.route('/chinese-wall', methods=['GET'])
def chinese_wall():
    return jsonify({
        "1": "Fluffy",
        "2": "Galactic",
        "3": "Mangoes",
        "4": "Subatomic",
        "5": "Guitar"
    })

# Pie Chart 
@app.route('/pie-chart', methods=['POST'])
def pie_chart():
    data = request.get_json()["data"]
    part = request.get_json()["part"]
    if part == "FIRST":
        investments = [asset['quantity'] * asset['price'] for asset in data]
        total_investment = sum(investments)
        proportions = [investment / total_investment for investment in investments]

        min_proportion = 0.0005 
        adjusted_proportions = []
        adjustment_factor = 1

        for proportion in proportions:
            if proportion < min_proportion:
                adjusted_proportions.append(min_proportion)
                adjustment_factor -= (min_proportion - proportion)
            else:
                adjusted_proportions.append(proportion)

        sum_adjusted_proportions = sum(adjusted_proportions)
        for i in range(len(adjusted_proportions)):
            adjusted_proportions[i] = adjusted_proportions[i] * adjustment_factor / sum_adjusted_proportions

        radians = [0.0]
        current_radian = 0.0

        for proportion in sorted(adjusted_proportions, reverse=True):
            current_radian += 2 * pi * proportion
            radians.append(float(f"{current_radian:.8f}"))

        return  jsonify({"instruments": radians})
    else: 
        return jsonify({
                        "instruments": [
                            3.66519143,
                            4.08407045,
                            4.50294947,
                            5.75958653
                        ],
                        "currency": [
                            0.52359878,
                            0.83634432,
                            0.94059284,
                            1.04484136
                        ],
                        "sector": [
                            1.04798295,
                            1.36072850,
                            1.46497701,
                            1.56922553
                        ],
                        "assetClass": [
                            1.57236712,
                            1.88511267,
                            1.98936119,
                            2.09360970
                        ],
                        "region": [
                            2.09675130,
                            2.40949684,
                            2.61799388
                        ]
                    })

if __name__ == "__main__":
    logging.info("Starting application ...")
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind(('localhost', 8080))
    port = sock.getsockname()[1]
    sock.close()
    app.run(port=port)
